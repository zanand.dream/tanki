#pragma once 
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "commands/MoveCommand.hpp"
#include "core/IMovable.hpp"
#include "utils.hpp"

using namespace my_game;

template <Arithmetic T, unsigned int N>
class MockMovable : public IMovable<T, N> {
public:
  MOCK_METHOD((std::array<T, N> &), get_position, (), (override));
  MOCK_METHOD((std::array<T, N> const &), get_position, (), (const override));
  MOCK_METHOD(void, set_position, ((std::array<T, N> postion)), (override));

  MOCK_METHOD((std::array<T, N> &), get_velocity, (), (override));
  MOCK_METHOD((std::array<T, N> const &), get_velocity, (), (const override));
  MOCK_METHOD(void, set_velocity, ((std::array<T, N> position)), (override));
};

using MockMovable3d = MockMovable<double, 3>;