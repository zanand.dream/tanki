#include "MockMovable.hpp"
#include "gtest/gtest.h"
#include <gmock/gmock-actions.h>
#include <gmock/gmock-function-mocker.h>
#include <gmock/gmock-spec-builders.h>
#include <memory>
#include "collusion/CollusionResolver.hpp"
#include "commands/CheckCollusionCommand.hpp"
#include "core/IOC.hpp"
using namespace my_game;
using namespace std;

class MockCollusionResolver: public CollusionResolver {
  public:
  MOCK_METHOD((bool), is_collusion, (IMovable3d const&, IMovable3d const&), (override));
};

using ::testing::_;

void ioc_init(shared_ptr<CollusionResolver> resolver){
  IOC.resolve<Nothing>("register", {{"name", std::string("get_collusion_resolver")}, {"strategy", strategy_fun{[=](args const&){
    return resolver;
  }}}});
}

TEST(CollusionCommandTest, do_nothing_if_no_collusion){
  MockMovable3d first;
  MockMovable3d second;
  auto resolver = make_shared<MockCollusionResolver>();
  ioc_init(resolver);

  EXPECT_CALL(*resolver, is_collusion(_, _)).WillOnce(testing::Return(false));

  CheckCollusionCommand command(first, second);

  command.execute();
}

TEST(CollusionCommandTest, throw_if_collusion){

  MockMovable3d first;
  MockMovable3d second;
  auto resolver = make_shared<MockCollusionResolver>();
  ioc_init(resolver);

  EXPECT_CALL(*resolver, is_collusion(_, _)).WillOnce(testing::Return(true));

  CheckCollusionCommand command(first, second);

  EXPECT_THROW(command.execute(), CollusionException);
  
}