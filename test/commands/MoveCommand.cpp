#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <array>

#include "commands/MoveCommand.hpp"
#include "core/IMovable.hpp"
#include "utils.hpp"

#include "MockMovable.hpp"

using ::testing::ReturnRef;

using namespace my_game;



TEST(MoveCommandsTest, StartMoveTest) {
  MockMovable3d movable;

  EXPECT_CALL(movable, set_velocity(std::array<double, 3>{1.0, 0.0, 0.0}))
      .Times(1);

  StartMoveCommand3d command(&movable, {1.0, 0.0, 0.0});
  command.execute();
}

TEST(MoveCommandsTest, MoveTest) {
  MockMovable3d movable;

  auto velocity = std::array<double, 3>{1.0, 3242.3, 123132222556.0};
  auto position = std::array<double, 3>{234., 256., 312.};

  auto start_position = position;
  auto end_position = position;

  for (unsigned int i = 0; i < 3; i++) {
    end_position[i] += velocity[i];
  }

  EXPECT_CALL(movable, get_velocity()).WillOnce(ReturnRef(velocity));
  EXPECT_CALL(movable, get_position()).WillOnce(ReturnRef(position));

  MoveCommand3d command(&movable);
  command.execute();

  EXPECT_EQ(position, end_position);
  EXPECT_NE(position, start_position);
}

TEST(MoveCommandsTest, EndMoveTest) {
  MockMovable3d movable;

  EXPECT_CALL(movable, set_velocity(std::array<double, 3>{0.0})).Times(1);

  EndMoveCommand3d command(&movable);
  command.execute();
}