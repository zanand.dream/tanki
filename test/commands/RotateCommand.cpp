#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <array>
#include <gmock/gmock-spec-builders.h>
#include <numbers>
#include "commands/RotateCommand.hpp"
#include "core/IRotatable.hpp"
#include "utils.hpp"

using ::testing::ReturnRef;
using ::testing::Return;
using namespace my_game;

template<Arithmetic T, unsigned int N>
class MockRotatable: public IRotatable<T, N> {
  public:
  MOCK_METHOD((std::array<T, N> &), get_direction, (), (override));
  MOCK_METHOD((std::array<T, N> const&), get_direction, (), (const override));
  MOCK_METHOD(void, set_direction, ((std::array<T, N> direction)), (override));
  MOCK_METHOD(double, get_angle_velocity, (), (const override));
  MOCK_METHOD(void, set_angle_velocity, (double angle), (override) );
};

using MockRotatable3d = MockRotatable<double, 3>;

TEST(RotateCommandsTest, StartRotateCommandTest) {
  MockRotatable3d mock;
  double angle = std::numbers::pi;
  EXPECT_CALL(mock, set_angle_velocity(angle)).Times(1);

  StartRotateCommand3d command(&mock, angle);
  command.execute();
}
// TODO: double compare function
TEST(RotateCommandsTest, RotateCommandTest) {
  MockRotatable3d mock;
  auto direction = std::array<double, 3>{1., 0., 0.};
  double angle = std::numbers::pi;

  auto result = direction;
  result[0] = cos(angle) * direction[0] - sin(angle) * direction[1];
  result[1] = sin(angle) * direction[0] + cos(angle) * direction[1];

  RotateCommand3d command(&mock);

  EXPECT_CALL(mock, get_direction()).WillOnce(ReturnRef(direction));
  EXPECT_CALL(mock, get_angle_velocity()).WillOnce(Return(angle));


  command.execute();
  EXPECT_EQ(direction, result);
}

TEST(RotateCommandsTest, EndRotateCommandTest) {
  MockRotatable3d mock;
  double angle = 0.0;
  EXPECT_CALL(mock, set_angle_velocity(angle)).Times(1);

  EndRotateCommand3d command(&mock);
  command.execute();
}
