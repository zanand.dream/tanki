#include "core/IOC.hpp"
#include "commands/FunctionCommand.hpp"
#include "commands/MoveCommand.hpp"
#include "utils.hpp"
#include "gtest/gtest.h"
#include <exception>
#include <memory>
#include <queue>
#include <string>
#include <typeinfo>
#include <unordered_map>

using namespace my_game;
using namespace std;

TEST(IOCtest, register_test) {
  ioc test_ioc{};
  ioc_state state = test_ioc.get_state();

  // initial ioc should have register strategy
  ASSERT_TRUE(state.contains("register"));

  // it should take params
  EXPECT_THROW(test_ioc.resolve<Nothing>("register"), IOC_wrong_params);

  args wrong_params{{"name", "i should be a std::string type, but im a char*"},
                    {"strategy", []() {
                       // i should be a std::function type, but im a lambda
                       return none;
                     }}};
  // you should use only std::string as name and std::function as strategy
  EXPECT_THROW(test_ioc.resolve<Nothing>("register", wrong_params),
               IOC_bad_params_types);

  args good_params{
      {"name", std::string("uwu")},
      {"strategy", strategy_fun{[](args const &) { return int(42); }}}};

  // should return Nothing
  EXPECT_THROW(test_ioc.resolve<int>("register", good_params),
               IOC_bad_return_type);

  // should work correctly
  test_ioc.resolve<Nothing>("register", good_params);
  EXPECT_EQ(test_ioc.resolve<int>("uwu"), int(42));
}

TEST(IOCtest, save_state_test) {
  ioc test_ioc;
  // state should return ioc state
  EXPECT_EQ(typeid(test_ioc.get_state()), typeid(ioc_state));
}

TEST(IOCtest, resolve_test) {
  ioc test_ioc;

  EXPECT_THROW(test_ioc.resolve<Nothing>("im not exist"),
               IOC_strategy_not_exist);
  args args_for_new_command{{"name", std::string("uwu")},
                            {"strategy", strategy_fun{[](args const &) {
                               return std::string("uwu");
                             }}}};
  test_ioc.resolve<Nothing>("register", args_for_new_command);

  EXPECT_THROW(test_ioc.resolve<Nothing>("uwu"), IOC_bad_return_type);
  EXPECT_EQ(test_ioc.resolve<std::string>("uwu"), std::string("uwu"));
}

TEST(IOCtest, get_strategy_test) {
  ioc test_ioc;

  EXPECT_THROW(test_ioc.get_strategy("im not exist for sure"),
               IOC_strategy_not_exist);

  args args_for_new_command{
      {"name", std::string("reeee")},
      {"strategy", strategy_fun{[](args const &) { return int(42); }}}};
  test_ioc.resolve<Nothing>("register", args_for_new_command);

  strategy_fun strategy = test_ioc.get_strategy("reeee");
  EXPECT_EQ(test_ioc.resolve<int>("reeee"), any_cast<int>(strategy({{}})));
}

TEST(IOCtest, exception_handling) {
  ioc test_ioc;

  // EXAMPLE OF EXCEPTION HANDLING WITH IOC
  IOC.resolve<Nothing>(
      "register",
      {{"name", std::string("exception_handle")},
       {"strategy", strategy_fun{[](args const &params) {
          try {

            // checking for existing of parameters
            std::exception const &except =
                any_cast<std::exception const &>(params.at("exception"));
            const shared_ptr<ICommand> e_command =
                any_cast<shared_ptr<ICommand>>(params.at("command"));

            shared_ptr<ICommand> result_command{};

            if (typeid(FunctionCommand) == params.at("command").type()) {
              result_command = std::make_shared<FunctionCommand>(
                  [&]() { throw MyException(); });
            } else
              result_command =
                  std::make_shared<FunctionCommand>([&]() { std::rethrow_exception(std::current_exception()); });

            return result_command;

          } catch (bad_any_cast e) {
            throw IOC_bad_params_types();
          } catch (out_of_range e) {
            throw IOC_wrong_params();
          }
        }}}});

  // example of usage
  std::queue<shared_ptr<ICommand>> queue;
  queue.push(make_shared<FunctionCommand>([]() { throw MyException(); }));

  while (!queue.empty()) {
    auto command = queue.front();
    queue.pop();
    try {
      command->execute();
    } catch (std::exception &e) {
      EXPECT_THROW(
          IOC.resolve<shared_ptr<ICommand>>(
                 "exception_handle", {{"exception", e}, {"command", command}})
              ->execute(),
          MyException);
    }
  }
}
