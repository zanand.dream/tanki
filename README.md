# REQUIREMENTS
To build this you need:
* Cmake >= 3.16
* Conan 
* On Linux: gcc>=10 or clang>=12

# HOW TO BUILD
```
    mkdir out
    cd out
```
on windows 
```
    conan install .. --build missing --profile ..\profiles\conan_profile_windows.txt
    cmake install ..
    // open visual studio and make it build it (dont know how to do it in command line)
```
linux:
```
    conan install .. --build missing --profile ../profiles/conan_profile_linux.txt
    cmake install ..
    // if you use clangd as language server, pass to cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=1
    // and create link to compile_commands.json ln -s out/compile_commands.json ./compile_commands.json
    make all
```


