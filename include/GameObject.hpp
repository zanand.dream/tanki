#pragma once
#include "core/IGameObject.hpp"
#include <any>
#include <string>
#include <unordered_map>

namespace my_game {

class GameObject : public IGameObject {
  std::unordered_map<std::string, std::any> values;

public:
  void set_property(std::string const &name, std::any value) override;
  std::any &get_property(std::string const &name) override;
  std::any const &get_property(std::string const &name) const override;
};

} // namespace my_game
