#pragma once

#include "fmt/core.h"
#include "utils.hpp"
#include <any>
#include <exception>
#include <functional>
#include <iostream>
#include <optional>
#include <stdexcept>
#include <string>
#include <unordered_map>

namespace my_game {

// TODO: move exceptions to another file
class IOC_exception : public MyException {
  // TODO: save state of ioc
};

class IOC_strategy_not_exist : public MyException {
  std::string message;

public:
  IOC_strategy_not_exist(std::string const &s) : message(s) {}
  const char *what() const noexcept override { return message.c_str(); }
};

class IOC_bad_return_type : public MyException {
  const char *what() const noexcept override { return "cant any_cast value from strategy "; }
};

class IOC_bad_params_types : public MyException {
  const char *what() const noexcept override { return "cant any_cast input params"; }
};

class IOC_wrong_params : public MyException {
  const char *what() const noexcept override { return "cant find needed input params"; }
};

using args = std::unordered_map<std::string, std::any>;
using strategy_fun = std::function<std::any(args const &)>;
using ioc_state = std::unordered_map<std::string, strategy_fun>;

// i cant return void in functions, so all functions should return Nothing!
class Nothing {}; 
static Nothing none{};

const args empty{};

class ioc {
  std::unordered_map<std::string, strategy_fun> strategys;

public:

  ioc() : strategys(std::unordered_map<std::string, strategy_fun>()) {

    this->strategys["register"] = [&](args const &arguments) {
      try {
        const std::string strategy_name =
            std::any_cast<std::string>(arguments.at("name"));

        strategy_fun strategy =
            std::any_cast<strategy_fun>(arguments.at("strategy"));
        this->strategys[strategy_name] = strategy;
      } catch (std::bad_any_cast) {
        throw IOC_bad_params_types();
      } catch (std::out_of_range) {
        throw IOC_wrong_params();
      }
      return none;
    };

  }

  template <class T>
  T resolve(std::string const &strategy, args const &arguments = empty) {
    try {

      return std::any_cast<T>(this->strategys.at(strategy)(arguments));

    } catch (std::out_of_range) {
      throw IOC_strategy_not_exist{
          fmt::format("strategy: <{}> does not exist", strategy)};
    } catch (std::bad_any_cast) {
      throw IOC_bad_return_type();
    }
  }

  ioc_state get_state() { return this->strategys; }

  //
  strategy_fun get_strategy(std::string strategy_name){
    try {
      return this->strategys.at(strategy_name);
    } catch (std::out_of_range) {
      throw IOC_strategy_not_exist(fmt::format("strategy: <{}> does not exist", strategy_name));
    }
  }
};

thread_local static ioc IOC{};

} // namespace my_game