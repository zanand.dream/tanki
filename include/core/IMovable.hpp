#pragma once
#include "utils.hpp"
#include <array>

namespace my_game {

/**
 * @brief Abstract class of movable object
 *
 * @tparam T - Type of element in vector
 * @tparam N - Dimension of this vector
 */
template <Arithmetic T, unsigned int N> class IMovable {

public:
  virtual std::array<T, N> &get_position() = 0;
  virtual std::array<T, N> const &get_position() const = 0;
  virtual void set_position(std::array<T, N> position) = 0;

  virtual std::array<T, N> &get_velocity() = 0;
  virtual std::array<T, N> const &get_velocity() const = 0;
  virtual void set_velocity(std::array<T, N> velocity) = 0;

  virtual ~IMovable() = default;
};

using IMovable3d = IMovable<double, 3>;

} // namespace my_game
