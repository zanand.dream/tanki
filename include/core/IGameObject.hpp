#pragma once

#include <any>
#include <string>

namespace my_game {

class IGameObject {
public:
  virtual void set_property(std::string const &name, std::any value) = 0;
  virtual std::any &get_property(std::string const &name) = 0;
  virtual std::any const &get_property(std::string const &name) const = 0;

  virtual ~IGameObject() = default;
};

} // namespace my_game
