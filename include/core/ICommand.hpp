#pragma once

namespace my_game {

class ICommand {
public:
  virtual void execute() = 0;

  virtual ~ICommand() = default;
};

}; // namespace my_game
