#pragma once
#include "utils.hpp"
#include <array>

namespace my_game {

/**
  @brief Represent Rotatable object on xy plane

  @tparam T - Type of element of vector
  @tparam N - Dimension of this vector
*/
template <Arithmetic T, unsigned int N> class IRotatable {
public:
  virtual std::array<T, N> &get_direction() = 0;
  virtual std::array<T, N> const &get_direction() const = 0;
  virtual void set_direction(std::array<T, N> direction) = 0;

  virtual double get_angle_velocity() const = 0;
  virtual void set_angle_velocity(double angle) = 0;

  virtual ~IRotatable() = default;
};

using IRotatable3d = IRotatable<double, 3>;

} // namespace my_game
