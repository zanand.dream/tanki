#pragma once

#include "commands/MoveCommand.hpp"
#include "core/ICommand.hpp"
#include "core/IMovable.hpp"
#include "core/IOC.hpp"
#include "utils.hpp"
#include <array>

namespace my_game {

template <Arithmetic T, unsigned int N> class ShootCommand : public ICommand {
  IMovable<T, N> &object;

public:
  ShootCommand(IMovable<T, N> &shoot_obj)
      : object(shoot_obj) {
      }

  void execute() override {
    auto const& position = this->object.get_position();
        IMovable<T, N>* bullet = IOC.resolve<IMovable<T,N>*>("Create.IMovable", {{"position", position}});
        bullet->set_velocity(std::array<T, N>{1.});
        
        MoveCommand<T, N> command (bullet);
        IOC.resolve<ICommand*>("Command.AddToQueue", {{"command", command}})->execute();
  }
};

} // namespace my_game
