#pragma once

#include "core/ICommand.hpp"
#include "core/IRotatable.hpp"
#include "utils.hpp"
#include <array>
#include <cmath>

namespace my_game {

/**
 * @brief Rotates object on the xy plane
 *
 * @tparam T - Type of element of vector
 * @tparam N - Dimension of this vector
 */
template <Arithmetic T, unsigned int N> class RotateCommand : public ICommand {
  IRotatable<T, N> *object;

public:
  RotateCommand(IRotatable<T, N> *obj) : object(obj) {}

  void execute() override {
    if constexpr (N == 0)
      return;
    auto angle = this->object->get_angle_velocity();
    auto &direction = this->object->get_direction();

    if constexpr (N >= 2) {
      auto temp_x = direction[0];
      direction[0] = cos(angle) * direction[0] - sin(angle) * direction[1];
      direction[1] = sin(angle) * temp_x + cos(angle) * direction[1];
    } else {
      direction[0] *= cos(angle);
    }

    // this->object->set_direction(std::move(direction));
  }
};

using RotateCommand3d = RotateCommand<double, 3>;

/**
 * @brief Set an initial angle velocity
 *
 * @tparam T - Type of element of vector
 * @tparam N - Dimension of this vector
 */
template <Arithmetic T, unsigned int N>
class StartRotateCommand : public ICommand {
  IRotatable<T, N> *object;
  const double angle;

public:
  StartRotateCommand(IRotatable<T, N> *obj, double angle_velocity)
      : object(obj), angle(angle_velocity) {}

  void execute() override { this->object->set_angle_velocity(angle); }
};

using StartRotateCommand3d = StartRotateCommand<double, 3>;

/**
 * @brief Set an angle velocity to zero
 *
 * @tparam T - Type of element of vector
 * @tparam N - Dimension of this vector
 */
template <Arithmetic T, unsigned int N>
class EndRotateCommand : public ICommand {
  IRotatable<T, N> *object;

public:
  EndRotateCommand(IRotatable<T, N> *obj) : object(obj) {}

  void execute() override { this->object->set_angle_velocity(0.0); }
};

using EndRotateCommand3d = EndRotateCommand<double, 3>;

} // namespace my_game
