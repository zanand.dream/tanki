#pragma once

#include "core/ICommand.hpp"
#include <vector>

namespace my_game {
/**
 * @brief Command which executes sequence of commands
 */
class MacroCommand : public ICommand {
  std::vector<ICommand *> &commands;

public:
  MacroCommand(std::vector<ICommand *> &command_queue)
      : commands(command_queue) {}

  void execute() override {
    for (auto command : this->commands) {
      // TODO: handle exceptions?
      command->execute();
    }
  }
};

} // namespace my_game