#pragma once

#include "collusion/CollusionResolver.hpp"
#include "core/ICommand.hpp"
#include "core/IMovable.hpp"
#include "core/IOC.hpp"
#include "utils.hpp"
#include <memory>
namespace my_game {

class CollusionException : public MyException {};

class CheckCollusionCommand : public ICommand {
  IMovable3d const &first;
  IMovable3d const &second;

public:
  CheckCollusionCommand(IMovable3d const &_first, IMovable3d const &_second)
      : first(_first), second(_second) {}

  void execute() override {
    std::shared_ptr<CollusionResolver> resolver =
        IOC.resolve<std::shared_ptr<CollusionResolver>>(
            "get_collusion_resolver");

    bool is_collusion = resolver->is_collusion(this->first, this->second);

    if (is_collusion) {
      throw CollusionException();
    }
  }
};

} // namespace my_game