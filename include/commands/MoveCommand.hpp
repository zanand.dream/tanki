#pragma once

#include "core/ICommand.hpp"
#include "core/IMovable.hpp"
#include "utils.hpp"
#include <array>

namespace my_game {

/**
 * @brief Command that moving a object by its velocity vector
 *
 * @tparam T - Type of element of vector
 * @tparam N - Dimension of this vector
 */
template <Arithmetic T, unsigned int N> class MoveCommand : public ICommand {
  IMovable<T, N> *object;

public:
  MoveCommand(IMovable<T, N> *obj) : object(obj){};

  void execute() override {
    auto const &velocity = this->object->get_velocity();
    auto &position = this->object->get_position();

    for (unsigned int i = 0; i < N; i++) {
      position[i] += velocity[i];
    }
    // this->object->set_position(position);
  }
};

using MoveCommand3d = MoveCommand<double, 3>;

/**
 * @brief Command sets a start velocity to an object
 *
 * @tparam T - Type of element of vector
 * @tparam N - Dimension of this vector
 */
template <Arithmetic T, unsigned int N>
class StartMoveCommand : public ICommand {
  IMovable<T, N> *object;
  std::array<T, N> velocity;

public:
  StartMoveCommand(IMovable<T, N> *obj, std::array<T, N> velocity)
      : object(obj), velocity(velocity){};

  void execute() override { this->object->set_velocity(this->velocity); }
};

using StartMoveCommand3d = StartMoveCommand<double, 3>;

/**
 * @brief Command sets objects velocity to zero
 *
 * @tparam T - Type of element of vector
 * @tparam N - Dimension of this vector
 */
template <Arithmetic T, unsigned int N> class EndMoveCommand : public ICommand {
  IMovable<T, N> *object;

public:
  EndMoveCommand(IMovable<T, N> *obj) : object(obj) {}

  void execute() override { this->object->set_velocity(std::array<T, N>{0}); }
};

using EndMoveCommand3d = EndMoveCommand<double, 3>;

} // namespace my_game
