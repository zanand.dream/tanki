#pragma once

#include "core/ICommand.hpp"
#include "core/IMovable.hpp"
#include "core/IRotatable.hpp"
#include "utils.hpp"
#include <array>
#include <cmath>

namespace my_game {
/**
 * @brief Rotates velocity vector to the direction
 *
 * @tparam T - Type of element of vector
 * @tparam N - Dimension of this vector
 */
template <Arithmetic T, unsigned int N> class RenameMe : public ICommand {
  IMovable<T, N> *movable;
  IRotatable<T, N> *rotatable;

public:
  RenameMe(IMovable<T, N> *mov_obj, IRotatable<T, N> *rot_obj)
      : movable(mov_obj), rotatable(rot_obj) {}

  void execute() override {
    if constexpr (N <= 1) {
      return;
    }
    auto &velocity = this->movable->get_velocity();
    auto &direction = this->rotatable->get_direction();
    
    auto zero_direction = std::array<T, N>{0.};
    zero_direction[0] = 1.;

    // cos(angle) = d * zd/ (|d||zd|) => angle = arccos(d[0] / 1)
    auto angle = std::acos(direction[0]);

    // rotates velocity vector
    auto temp_x = velocity[0];
    velocity[0] = cos(angle) * velocity[0] - sin(angle) * velocity[1];
    velocity[1] = sin(angle) * temp_x + cos(angle) * velocity[1];

    direction = zero_direction;
  }
};

using RenameMe3d = RenameMe<double, 3>;

} // namespace my_game