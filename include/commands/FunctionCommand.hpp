#pragma once

#include "core/ICommand.hpp"
#include <functional>
namespace my_game {

class FunctionCommand : public ICommand {
  std::function<void(void)> function;

public:
  FunctionCommand(std::function<void(void)> _func): function(_func) {}
  
  void execute() override {
    this->function();
  }
};

} // namespace my_game