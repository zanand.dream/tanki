#pragma once

#include "core/IMovable.hpp"
namespace my_game {

class CollusionResolver {
public:

  virtual ~CollusionResolver() = default;
  
  virtual bool is_collusion(IMovable3d const&, IMovable3d const&) = 0;
};

}