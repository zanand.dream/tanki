#pragma once

#include <exception>
#include <type_traits>

namespace my_game {

template <class T>
concept Arithmetic = std::is_arithmetic_v<T>;


class MyException: public std::exception {};
} // namespace my_game
