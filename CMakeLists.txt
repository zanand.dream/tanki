# PROJECT
	cmake_minimum_required(VERSION 3.18)
	project               (tanki)

	set(LIB_MAIN_NAME main_lib)
	set(EXE_MAIN_NAME main)
	# FIXME: somehow remove this hardcoded path (CMAKE_BIN_PATH doesnt work for some reason)
	set(BUILD_DIR ${PROJECT_SOURCE_DIR}/out)
	set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# OPTIONS 
	option(BUILD_TESTS "Build tests." ON)

# DEPENDENCIES 
	include(${BUILD_DIR}/conanbuildinfo.cmake)
	include(${BUILD_DIR}/conan_paths.cmake)
	conan_basic_setup(TARGETS)

	find_package(fmt )
	
	if (BUILD_TESTS) 
		find_package(GTest REQUIRED)
	endif()

	set(CONAN_LIB fmt::fmt)

# TARGETS 

	# main library
		# getting all source files in src folder except src/main.cpp
		file(GLOB_RECURSE LIB_SOURCE CONFIGURE_DEPENDS "src/*.cpp" "src/*.cxx" "src/*.cc")
		list(REMOVE_ITEM LIB_SOURCE "${PROJECT_SOURCE_DIR}/src/main.cpp")

		# creating library target
		add_library(${LIB_MAIN_NAME})
		target_include_directories(${LIB_MAIN_NAME} PUBLIC ${PROJECT_SOURCE_DIR}/include)
		target_sources(${LIB_MAIN_NAME} PRIVATE ${LIB_SOURCE})
		target_compile_features(${LIB_MAIN_NAME} PUBLIC cxx_std_20)
		# compile options 
		if(MSVC)
		target_compile_options(${LIB_MAIN_NAME} PUBLIC /W4 /WX)
		elseif(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
		target_compile_options(${LIB_MAIN_NAME} PUBLIC -Wall -Wextra -Wpedantic  -glldb)
		else()
		target_compile_options(${LIB_MAIN_NAME} PUBLIC -Wall -Wextra -Wpedantic)
		endif()
		# adding dependencies 
		target_link_libraries(${LIB_MAIN_NAME} PUBLIC ${CONAN_LIB})

	# main executable
		add_executable(${EXE_MAIN_NAME})
		target_sources(${EXE_MAIN_NAME} PRIVATE ${PROJECT_SOURCE_DIR}/src/main.cpp)
		target_link_libraries(${EXE_MAIN_NAME} PRIVATE ${LIB_MAIN_NAME})

# TESTS
if (BUILD_TESTS) 
    enable_testing() 
	file(GLOB_RECURSE TEST_SOURCE CONFIGURE_DEPENDS "test/*.cpp" "test/*.cxx" "test/*.cc")
    add_executable(main_test ${TEST_SOURCE})
    target_link_libraries(main_test PRIVATE ${LIB_MAIN_NAME} GTest::gtest GTest::gtest_main GTest::gmock_main GTest::gmock)
	target_include_directories(main_test PUBLIC ${PROJECT_SOURCE_DIR}/test/include)
    add_test(main_test main_test)
	
endif()
