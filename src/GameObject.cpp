#include "GameObject.hpp"

namespace my_game {
// TODO: handle exceptions ?

std::any &GameObject::get_property(std::string const &name) {
  return this->values.at(name);
}

std::any const &GameObject::get_property(std::string const &name) const {
  return this->values.at(name);
}

void GameObject::set_property(std::string const &name, std::any value) {
  this->values.at(name) = value;
}

} // namespace my_game
